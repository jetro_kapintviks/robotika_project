import matplotlib.pyplot as plt

from skimage.feature import hog
from skimage import data, color, exposure
import os,sys
import skimage
from sklearn import svm
import numpy as np
import pickle
from sklearn.linear_model import Ridge,RidgeClassifier,LogisticRegression
import random
import cv2
from skimage.feature import canny
from skimage.filters import roberts, sobel, scharr, prewitt
import random
import shutil
import time
from sklearn.decomposition import RandomizedPCA

kernel=np.zeros((13,13),dtype=np.uint8)
kernel[:,kernel.shape[1]//2]=1
kernel[kernel.shape[0]//2,:]=1

def remove_back(image):
    image_c=skimage.filters.gaussian(image, \
       sigma=0.6, output=None, mode='nearest')
    image_c=image
    #     image_c=cv2.GaussianBlur(image_c,(15,15),0)
    filtered=canny(image,sigma=0.252)# 0.052
#     print(filtered[:5,:5])
    filtered=filtered!=0
    edges =skimage.img_as_ubyte(filtered)
    edges=cv2.GaussianBlur(edges,(15,15),0)#,sigmaX=0.4,sigmaY=0.4)
#     edges=cv2.dilate(edges,kernel,iterations = 1)
    edges=cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel,iterations=3)
    #     edges=edges!=0.0

    #     image_min[result2==255]=0
    edges[edges==51]=0
    edges_c=edges.copy()
    mask = np.zeros((image_c.shape[0]+2,
               image_c.shape[1]+2), np.uint8)

    # Floodfill from point (0, 0)
    cv2.floodFill(edges, mask, (398,0), 51)
    cv2.floodFill(edges, mask, (385,10), 51)

    cv2.floodFill(edges, mask, (10,10), 51)
    cv2.floodFill(edges, mask, (5,5), 51)

    image_c[edges==51]=0

#     image_c=cv2.GaussianBlur(image_c,(3,3),0)
    return image_c,edges_c

class HandDetector:
    def __init__(self,classifier_file='classes.names',createNew=True):
        self.classifier_file=classifier_file
        self.finishedT=False
        self.classes_names=['loser_h',
                             'closed_h',
                             'devil_h',
                             'second_h',
                             'thumb_h',
                             'first_h',
                             'surfer_h',
                             'open_h']
        self.classes_i={x:i for i,x in enumerate(self.classes_names)}

        self.createNew2()
        print(self.classes_names,self.classes_i)
    def createNew2(self):
        with np.load('modelData_changing.npz') as dataXy:
            print('im in reading pickle')
            sys.stdout.flush()
            X=dataXy['X']
            y=dataXy['y']
        self.model=LogisticRegression()
        self.model.fit(X,y)
    def temp_wait_arb(self):
        self.get_MV_solution()
        time.sleep(random.randrange(1,3))
        self.finishedT=True
    def reset_Solutions(self):
        self.solutions=[]
        self.predictions=[]
        self.curSol=None
    def reset_finish(self):
        self.finishedT=False
    def get_MV_solution(self):
        if len(self.solutions)==0:
            return 'no Prediction'
        elif self.curSol is not None:
            return self.curSol
        ranStr=random.randrange(16**10)
        print([self.classes_names[s] for s in self.solutions],
        # X.shape,np.sum(self.model.predict(X)==y)/X.shape[0],
        # ranStr,self.model.decision_function(self.fd)
        )
        # skimage.io.imsave('test_hidden/test_{}.png'.format(ranStr),self.img)


        binc=np.bincount(self.solutions)
        classi=np.argmax(binc)
        print('solutions',[self.classes_names[ci] for ci,x in enumerate(binc)])
        print('predictions',self.predictions)
        self.predictions=np.array(self.predictions)




        if np.sum(self.predictions[:,0]>0.5)>self.predictions.shape[0]//3:
            self.curSol=self.classes_names[classi]
        else:
            self.curSol='Not Sure'

        print('cur sol',self.curSol,len(self.solutions),np.max(binc))

        return self.curSol

    def hasSolution(self):
        return self.curSol is not None
    def is_finished_talk(self):
        return self.finishedT
    def add_onePic(self,image):
        # skimage.io.imsave("img_{}.png".format(random.randrange(256)),image )
        image=color.rgb2gray(image)
        image,edge=remove_back(image)
        skimage.io.imsave('test_hidden/test_{}.png'.\
            format(random.randrange(16*10)),image)

        fd,self.img = hog(image, orientations=8, pixels_per_cell=(16, 16),
        cells_per_block=(1, 1),visualise=True )

        self.img=exposure.rescale_intensity(self.img, in_range=(0, 0.05))

        fd=fd.reshape(1,-1)
        self.fd=fd
        sol=self.predict_one(fd)
        self.solutions.append( self.classes_i[sol] )
        # print('pred type',type(self.predictions))

        self.predictions.append(
        [np.max(self.predict_one_proba(fd) ),
        np.max(self.predict_one_decision(fd) )
        ])
        return image
    def predict_one(self,vec_fd):
        # vec_fd=self.pcam.transform(vec_fd)
        return self.model.predict(vec_fd)[0]
    def predict_one_proba(self,vec_fd):
        # vec_fd=self.pcam.transform(vec_fd)
        return self.model.predict_proba(vec_fd)[0]
    def predict_one_decision(self,vec_fd):
        # vec_fd=self.pcam.transform(vec_fd)
        return self.model.decision_function(vec_fd)[0]
    def make_model(self):
        X=None
        y=np.empty((0,1),dtype=str)

        classP=[os.path.join('imgs',i) for i in os.listdir('imgs') if not i.startswith('.') ]

        for indc,classi in enumerate(classP):
            imgs=[os.path.join(classi,i) for i in os.listdir(classi) ]
            for indi,img in enumerate(imgs):

                if indi%140==0:
                    print("im in: ",indc,indi)#flush

                image = color.rgb2gray(skimage.io.imread(img))
        #         print(image.shape)
                fd = hog(image, orientations=8, pixels_per_cell=(16, 16),
                                    cells_per_block=(1, 1), visualise=False)

        #         print(np.vstack((fd,fd)).shape)
                if X is None:
                    X=np.zeros((0,fd.shape[0]))
                X=np.vstack((X,fd))

                y=np.vstack((y,classi[classi.rindex('/')+1:]))

                if indc==0 and indi==5:
                    plt.figure(figsize=(20,20))

                    # Rescale histogram for better display
                    hog_image_rescaled = \
                    exposure.rescale_intensity(hog_image, in_range=(0, 0.3))

                    plt.axis('off')
                    plt.imshow(hog_image, cmap=plt.cm.gray)
                    plt.title('Histogram of Oriented Gradients')
                    plt.show()

        y=y.flatten()

        print(X.shape,y.shape)
        np.random.seed(1)
        indx=np.arange(X.shape[0])
        np.random.shuffle(indx)

        X,y=X[indx ],y[indx ]

        clf = RidgeClassifier(alpha=1.0,)
        clf.fit(X,y)
        self.model=clf
        self.classes_names=[c[c.index('/')+1:] for c in classP]
