import cv2
import numpy as np
import math
import sys,os
import Tkinter as tk
import random
from skimage.feature import hog
from skimage import data, color, exposure
import sklearn
import skimage
import time

print('before hModel')
sys.stdout.flush()

from HandDetect2 import HandDetector
from Queue import Queue
import threading

low_limx=770
low_limy=400
upp_limx=1600
upp_limy=1000

def work_back(hModel):
    while True:
        q_val=savedImgs.get(True)
        if q_val is None:
            break
        elif q_val=='Talk':
            print('Really Talking!')
            hModel.temp_wait_arb()
        else:
            new_s,flipV=q_val
            if flipV:
                new_s=cv2.flip(new_s,1)
            hModel.add_onePic(new_s)
        savedImgs.task_done()
    savedImgs.task_done()

def updateValue(event):
    w = event.widget
    global low_limx,low_limy,upp_limx,upp_limy
    if isinstance(w, tk.Scale) and w.cget('label')=='rectangle size':
        upp_limx=low_limx+w.get()
        upp_limy=low_limy+w.get()
    if isinstance(w, tk.Scale) and w.cget('label')=='rectangle height':
        upp_limy=low_limy+w.get()

    if isinstance(w, tk.Scale) and w.cget('label')=='rectangle posx':
        upp_limx-=low_limx
        low_limx=w.get()
        upp_limx+=low_limx
    if isinstance(w, tk.Scale) and w.cget('label')=='rectangle posy':
        upp_limy-=low_limy
        low_limy=w.get()
        upp_limy+=low_limy
def edited_txt(tead):
    print(tead.get())
EXP_TXT="""
Press s to start recording  in the choosen directory
Press e to stop recording
"""
def key(event):
    global tkK
    print ('pressed',repr(event.char),ord(event.char))
    tkK=ord(event.char)
def init_tk():
    global flipVal,showMode

    master = tk.Tk()

    w = tk.Label(master, text=EXP_TXT)
    w.pack()



    sv=tk.StringVar()
    sv.set("open_h")
    if not showMode:
        sv.trace("w",edited_txt)

        w = tk.Label(master, text="enter dir of recording (imgs/..)")
        w.pack()
        w=tk.Entry(master,textvariable=sv)
        w.pack()

    # master.minsize(width=400,height=600)
    w = tk.Scale(master, from_=1, to=fwidth-400,length=slidWidth,
    label='rectangle posx',orient=tk.HORIZONTAL)
    w.set(low_limx)
    w.pack()
    w.bind("<ButtonRelease-1>", updateValue)

    w = tk.Scale(master, from_=1, to=fheight-400,length=slidWidth,
    label='rectangle posy',orient=tk.HORIZONTAL)
    w.set(low_limy)
    w.pack()
    w.bind("<ButtonRelease-1>", updateValue)
    master.bind("<Key>", key)

    flipVal=tk.BooleanVar()
    c = tk.Checkbutton(master, text=" Flip Screen", variable=flipVal)
    c.pack()
    # w = tk.Scale(master, from_=0, to=fwidth,length=slidWidth,
    # label='rectangle size',orient=tk.HORIZONTAL,state=tk.DISABLED)
    # w.set(upp_limx-low_limx)
    # w.pack()
    # w.bind("<ButtonRelease-1>", updateValue)

    # w = tk.Scale(master, from_=0, to=fheight,length=slidWidth,
    # label='rectangle height',orient=tk.HORIZONTAL)
    # w.set(upp_limy-low_limy)
    # w.pack()
    # w.bind("<ButtonRelease-1>", updateValue)

    return master,sv
class Rec:
    SAVE_ON=1
    TEST_ON=2
    OFF=3

master=None
tkK=None

low_limx=50
low_limy=50
upp_limx=450
upp_limy=450
framei=0
slidWidth=500
start_rec=Rec.OFF
frameStep=4
rec_color={Rec.SAVE_ON:(0,255,0),Rec.TEST_ON:(0,255,0),
Rec.OFF:(0,2,0)}
cap = cv2.VideoCapture(0)
fps=cap.get(cv2.cv.CV_CAP_PROP_FPS) or 23
if fps<=0: fps=23
fpi=2#int( fps*1.6 )

overH_l=skimage.io.imread('small_left.jpg')
overH_r=skimage.io.imread('small_right.jpg')

overH=overH_l

widthH=int((399-overH.shape[0])/2)
heightH=int((399-overH.shape[1])/2)
alpha=0.1
imgPad=None
imgOvr=None

showMode=True
batchHSize=0

fpp=int(fps*10)
# Rec Time
fpr=int(fps*2.5)
framep=0

time_start=0
curPred='no Prediction'

print('before hModel')
sys.stdout.flush()
hModel=HandDetector('handRidge_back.modelD')


savedImgs=Queue()
print('fps is ',fps)
#
# overH=cv2.copyMakeBorder(overH,heightH,overH.shape[1]-heightH,
# widthH,overH.shape[0]-widthH,cv2.BORDER_CONSTANT,(0,0,0,0))
print(overH.shape)
# cv2.imshow('g',overH)
# skimage.io.imsave('test.png',overH)
# cv2.waitKey(0)
while(cap.isOpened()):
    if master is None:
        fwidth=cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH)
        fheight=cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT)
        master,rec_dir=init_tk()
    master.update()

    ret,img=cap.read()

    if flipVal.get():
        img=cv2.flip(img,1)
        overH=overH_r
    else:
        overH=overH_l


    imgOvr=img.copy()
    imgOvr[low_limy+1:upp_limy, low_limx+1:upp_limx]=overH




    crop_img = img[low_limy+1:upp_limy, low_limx+1:upp_limx]
    grey =color.rgb2gray(crop_img)
    if np.prod(grey.shape)!=0:
        # fd, grey = hog(grey, orientations=8, pixels_per_cell=(16, 16),\
        #                     cells_per_block=(1, 1), visualise=True)
        # grey=exposure.rescale_intensity(grey, in_range=(0, 0.05))
        # cv2.imshow("HOG Coef.",grey)
        # print(str(np.sum(fd==0.0)),np.sum(fd!=0.0))
        pass


        # print(framei)
    if start_rec==Rec.TEST_ON and framep==0:
        curPred=hModel.get_MV_solution()+" Rec.."
        hModel.reset_Solutions()
        savedImgs=Queue()
        thread_back=threading.Thread(target=work_back,args=(hModel,))
        thread_back.start()
        # curPred='Recording...'
        # pass
    elif start_rec==Rec.TEST_ON and hModel.is_finished_talk():#framep==fpp-1:
        # curPred='not Ready...'
        savedImgs.put(None)
        savedImgs.join()
        thread_back.join()

        curPred=hModel.get_MV_solution()
        hModel.reset_finish()
        framep=fpp-1
        # print(hModel.hasSolution())


# =============================================

    rec_C=rec_color[start_rec]
    if start_rec!=Rec.OFF and framei==0:
        s_crop_img=crop_img.astype(int)
        stime=time.time()
        if not showMode: rec_C=rec_color[Rec.OFF]
    elif  start_rec!=Rec.OFF and framei==fpi-1:
        # cv2.imwrite(os.path.join(sav_dir,
        # 'img_{:03}.png'.format(random.randrange(16**10))),grey)
        s_crop_img+=crop_img
        s_crop_img=(s_crop_img// int(fpi)).astype(np.uint8)
        batchHSize+=1
        print(framei,'time: ',round(time.time()-stime,2),'batchHSize: ',batchHSize)

        s_crop_img=cv2.medianBlur(s_crop_img,7)

        if start_rec==Rec.SAVE_ON:
            if flipVal.get():
                s_crop_img=cv2.flip(s_crop_img,1)
            skimage.io.imsave(os.path.join(sav_dir,
            'rimg_{:03}.png'.format(random.randrange(16**10))),s_crop_img)
        elif start_rec==Rec.TEST_ON and framep<fpr:
            savedImgs.put(  (s_crop_img,flipVal.get() ) )
        elif start_rec==Rec.TEST_ON and framep>=fpr:
            if curPred!='Talking With Robot':
                savedImgs.put('Talk')
            curPred='Talking With Robot'
    elif start_rec!=Rec.OFF:
        s_crop_img+=crop_img

# ==============================================

    if start_rec==Rec.TEST_ON:
        cv2.putText(img,curPred, (20,100),\
            cv2.FONT_HERSHEY_SIMPLEX, 0.5,(255,255,255),2)
        dTime=int(time.time()-time_start)
        # if not hModel.hasSolution():
        #     cv2.putText(img,'time: {:02d}:{:02d} sec'.format(dTime//60,dTime%60),(150,100),
        #     cv2.FONT_HERSHEY_SIMPLEX,0.5,(255,255,255),2)


# ==============================================================

    cv2.rectangle(img,(upp_limx,upp_limy),(low_limx,low_limy),rec_C,0)
    cv2.addWeighted(imgOvr, alpha, img, 1 - alpha,
    	0, img)

    cv2.imshow("main img",img)


    k = cv2.waitKey(15)
    # if k!=-1: print(k)

    if tkK is not None and tkK in [13,27]:
        print(tkK)
        k=tkK
        tkK=None

    if k == 27:
        cv2.destroyAllWindows()
        break
    if k==115 or (k==13 and start_rec==Rec.OFF):
        if showMode:
            start_rec=Rec.TEST_ON
        else:
            start_rec=Rec.SAVE_ON
        print('started rec at dir: ',rec_dir.get())
        sav_dir=os.path.join('.','temp_imgs',rec_dir.get())
        if not os.path.exists(sav_dir): os.makedirs(sav_dir)
        sframei=framei-1
        framei=-1
        batchHSize=0

        hModel.reset_Solutions()
        framep=-1

        time_start=time.time()
    elif k==101 or (k==13 and start_rec!=Rec.OFF):
        if start_rec==Rec.TEST_ON:
            savedImgs.put(None)
            savedImgs.join()
            thread_back.join()
            hModel.reset_finish()

            print('now really closed!!')

        start_rec=Rec.OFF
        print('ended rec at dir: ',rec_dir.get())
    framei+=1
    framei%=fpi

    framep+=1
    framep%=fpp
