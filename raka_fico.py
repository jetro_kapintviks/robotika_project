import cv2
import numpy as np
import math
import sys,os


colors=[[188, 218, 165],
       [174, 153,   8],
       [ 34,  39,  40],
       [  1, 234, 203],
       [ 38, 212,  49],
       [211, 243,  34],
       [254, 186,  74],
       [ 64,  98,  68],
       [225,  75, 170],
       [217,   5,   2],
       [121, 118, 174],
       [204, 210, 159],
       [192, 138, 106],
       [219,  40,  15],
       [129,  52, 113],
       [ 47, 201, 204],
       [ 84, 147, 155],
       [ 53,   9, 141],
       [166, 151,  70],
       [165,  29, 173]]
low_limx=770
low_limy=400
upp_limx=1600
upp_limy=1000

fwidth=cap.get(cv2.CAP_PROP_FRAME_WIDTH)
fheight=cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

print(sys.executable,flush=True)
# cap = cv2.VideoCapture(0)
cap = cv2.VideoCapture('3.webm')
print(cap.isOpened(),os.getcwd())
while(cap.isOpened()):
    ret, img = cap.read()
    cv2.rectangle(img,(upp_limx,upp_limy),(low_limx,low_limy),(0,255,0),0)
    crop_img = img[low_limy:upp_limy, low_limx:upp_limx]
    grey = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
    value = (15, 15)
    blurred = cv2.GaussianBlur(grey, value, 0)
    _, thresh1 = cv2.threshold(blurred, 70, 255,
                               cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    cv2.imshow('Thresholded', thresh1)
    _,contours, hierarchy = cv2.findContours(thresh1.copy(),cv2.RETR_TREE, \
            cv2.CHAIN_APPROX_NONE)
    max_area = -1
    for i in range(len(contours)):
        cnt=contours[i]
        area = cv2.contourArea(cnt)
        if(area>max_area):
            max_area=area
            ci=i
    cnt=contours[ci]
    x,y,w,h = cv2.boundingRect(cnt)
    cv2.rectangle(crop_img,(x,y),(x+w,y+h),(0,0,255),0)
    hull = cv2.convexHull(cnt)
    drawing = np.zeros(crop_img.shape,np.uint8)
    cv2.drawContours(drawing,[cnt],0,(0,255,0),0)
    cv2.drawContours(drawing,[hull],0,(0,0,255),0)
    cnt = cv2.approxPolyDP(cnt,0.01*cv2.arcLength(cnt,True),True)
    hull = cv2.convexHull(cnt,returnPoints = False)
    moments = cv2.moments(cnt)
    if moments['m00']!=0:
                cx = int(moments['m10']/moments['m00']) # cx = M10/M00
                cy = int(moments['m01']/moments['m00']) # cy = M01/M00

    centr=(cx,cy)
    cv2.circle(crop_img,centr,5,[0,0,255],2)
    defects = cv2.convexityDefects(cnt,hull)
    count_defects = 0
    cv2.drawContours(thresh1, contours, -1, (0,255,0), 3)
    if defects is not None:
        for i in range(defects.shape[0]):
            s,e,f,d = defects[i,0]
            start = tuple(cnt[s][0])
            end = tuple(cnt[e][0])
            far = tuple(cnt[f][0])
            #dist = cv2.pointPolygonTest(cnt,far,True)


            # --- CRTANJE LINIJA OD EDEN VRV NA PRST DO DRUG ---
            cv2.line(crop_img,start,end,[0,255,0],2)

            # --- CRTANJE LINIJA OD VRVOT NA PRSTOT DO SREDINA NA DLANKATA ---
            cv2.line(crop_img,start,centr,[0,255,0],1)


            cv2.circle(crop_img,start,5,colors[i%20],-1)
            cv2.circle(crop_img,far,5,colors[i%20],-1)

    # cv2.putText(img,str(), (50,50),\
                # cv2.FONT_HERSHEY_SIMPLEX, 2, 2)
    cv2.imshow('Gesture', img)
    all_img = np.hstack((drawing, crop_img))
    cv2.imshow('Contours', all_img)
    k = cv2.waitKey(80)

    if k == 27:
        cv2.destroyAllWindows()
        break
