import numpy as np

from skimage.feature import canny

import cv2,skimage.io,random
import skimage
import skimage.filters


def remove_back(image):
    image_c=skimage.filters.gaussian(image, \
       sigma=0.825, output=None, mode='nearest')

    #     image_c=cv2.GaussianBlur(image_c,(15,15),0)
    edges =skimage.img_as_ubyte(skimage.feature.canny(image_c))
    edges=cv2.GaussianBlur(edges,(15,15),0)
    #     edges=edges!=0.0

    #     image_min[result2==255]=0


    mask = np.zeros((image_c.shape[0]+2,
               image_c.shape[1]+2), np.uint8)

    # Floodfill from point (0, 0)
    cv2.floodFill(edges, mask, (0,0), 51);

    image_c[edges==51]=0
    return image_c


cap = cv2.VideoCapture(0)
fgbg = cv2.createBackgroundSubtractorMOG2()

while(cap.isOpened()):
    ret, frame = cap.read()
    frame_m=cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frame_m=remove_back(frame_m)

    cv2.imshow('img',frame)
    cv2.imshow('no bord',frame_m)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
      break
    elif k==115:
      skimage.io.imsave("back_{}.png".format(random.randrange(256)),frame)


cap.release()
cv2.destroyAllWindows()
