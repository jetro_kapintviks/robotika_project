import numpy as np
import cv2,random,skimage.io

cap = cv2.VideoCapture('http://192.168.43.1:8080/video')
fgbg = cv2.createBackgroundSubtractorMOG2()

while(cap.isOpened()):
    ret, frame = cap.read()

    fgmask = fgbg.apply(frame)

    cv2.imshow('img',frame)
    cv2.imshow('frame',fgmask)


    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
    

cap.release()
cv2.destroyAllWindows()
