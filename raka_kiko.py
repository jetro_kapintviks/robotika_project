import cv2
import numpy as np                           #importing libraries
cap = cv2.VideoCapture(0)                #creating camera object
marea=0
while( cap.isOpened() ) :
  ret,img = cap.read()                         #reading the frames
  cv2.imshow('input',img)                  #displaying the frames
  k = cv2.waitKey(10)
  if k == 27:
    break
  gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
  blur=cv2.GaussianBlur(gray,(5,5),0)
  ret,thresh1 = cv2.threshold(blur,120,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
  cv2.imshow('Thresholded', thresh1)
  _,contours, hierarchy = cv2.findContours(thresh1,cv2.RETR_TREE,
  cv2.CHAIN_APPROX_NONE)
  for ind,cnt in enumerate(contours):
    area=cv2.contourArea(cnt)
    if area>marea:
        marea=area
        cind=ind
    hull=cv2.convexHull(cnt)
    drawing=np.zeros(img.shape,np.uint8)
    cv2.drawContours(drawing,[cnt],0,(0,255,0),2)
    cv2.drawContours(drawing,[hull],0,(0,0,255),2)
    cv2.imshow('Contours', drawing)
